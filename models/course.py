from odoo import models, fields, api, exceptions
class HrCourse(models.Model):
    _name = "hr.course"

    name = fields.Char('Name' ,required = True)
    start_date = fields.Date('Start Date' ,required = True)
    end_date = fields.Date('End Date' ,required = True)
    result = fields.Float('Result' , required = True)

    hr_id = fields.Many2one('hr.employee')

    @api.multi
    @api.depends('start_date','end_date')
    @api.constrains('start_date', 'end_date')
    def _check_date(self):
        for record in self:
            if record.end_date < record.start_date:
                raise exceptions.ValidationError(('Please Make Sure The Course Start Date Greater Than The End Date!'))

        # domain = [
        #     ('start_date', '=', record.start_date),
        #     ('end_date', '=', record.end_date),
        # ]
        # ncourse = self.search(domain)