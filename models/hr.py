from odoo import models, fields ,api
from datetime import  datetime
class HrEmployee(models.Model):
    _inherit = "hr.employee"

    age = fields.Integer("Age" , compute ="calc_age")

    course = fields.One2many('hr.course' , 'hr_id')

    @api.multi
    def calc_age(self):
        for rec in self:
            rec.age = (datetime.now() - datetime.strptime(rec.birthday,'%Y-%m-%d')).days / 365
